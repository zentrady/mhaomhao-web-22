import FontAwesomeIcon from '@fortawesome/fontawesome'
import { faFacebook, faInstagram, faTwitter, faYoutube } from '@fortawesome/free-brands-svg-icons'
import { faHome, faPhone, faBug, faLeaf, faAngleRight, faCheckCircle } from '@fortawesome/free-solid-svg-icons'

FontAwesomeIcon.config = {
  autoAddCss: false
}

FontAwesomeIcon.library.add(faFacebook, faInstagram, faTwitter, faYoutube, faHome, faPhone, faBug, faLeaf, faAngleRight, faCheckCircle)
